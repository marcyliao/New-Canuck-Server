drop database if exists newcanuck;
create database newcanuck;

use newcanuck;
CREATE TABLE `quizQuestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` mediumtext,
  `answerA` mediumtext,
  `answerB` mediumtext,
  `answerC` mediumtext,
  `answerD` mediumtext,
  `correctAnswer` mediumtext,
  `hint` mediumtext,
  `suggestion` mediumtext,
  `approval` varchar(45) DEFAULT 'false',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

CREATE TABLE `missions` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `create_date` mediumtext,
  `ratetimes` mediumtext,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `state` mediumtext,
  `img_file_name` text,
  `add_date` mediumtext,
  `complete_date` mediumtext,
  `rating` double DEFAULT NULL,
  `approval` varchar(45) DEFAULT 'false',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

CREATE TABLE `comments` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `mission_id` int(11) NOT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `user_rating` double DEFAULT NULL,
  `user_comment` text DEFAULT NULL,
  `create_date` mediumtext,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

INSERT INTO `newcanuck`.`missions`
(
`approval`,`_id`,`name`,`description`,`create_date`,`ratetimes`,`latitude`,`longitude`,`address`,`state`,`img_file_name`,`add_date`,`complete_date`,`rating`)
VALUES
(
'true',1,'Visit Niagara Falls', 'Niagara Falls is the collective name for three waterfalls that straddle the international border between the Canadian province of Ontario and the U.S. state of New York. They form the southern end of the Niagara Gorge.', '1361932008400', '1', '43.078966', '-79.078367', '(43.0779,-79.078)', '0', 'mission1', '1361932008400', '1361932008400', '3.5'
);

INSERT INTO `newcanuck`.`missions`
(
`approval`,`_id`,`name`,`description`,`create_date`,`ratetimes`,`latitude`,`longitude`,`address`,`state`,`img_file_name`,`add_date`,`complete_date`,`rating`)
VALUES
(
'true',2,'Visit CN Tower', 'The CN Tower is a 553.33 m-high concrete communications and observation tower in Downtown Toronto, Ontario, Canada. It was completed in 1976, becoming the world''s tallest free-standing structure and world''s tallest tower at the time.', '1361932008400', '3', '43.642609', '-79.387057', '(43.643,-79.387)', '0', 'mission2', '1361932008400', '1361932008400', '4'
);

INSERT INTO `newcanuck`.`missions`
(
`approval`,`_id`,`name`,`description`,`create_date`,`ratetimes`,`latitude`,`longitude`,`address`,`state`,`img_file_name`,`add_date`,`complete_date`,`rating`)
VALUES
(
'true',3,'Watch a Hockey Game', 'Ice hockey, referred to as simply  hockey;  is Canada''s most prevalent winter sport,[1] its most popular spectator sport, and its most successful sport in international competition. It is Canada''s official national winter sport.The Toronto Maple Leafs is a professional ice hockey franchise based in Toronto, Ontario, Canada. They are members of the Northeast Division of the Eastern Conference of the National Hockey League.', '1361932008400', '2', '43.641961', '-79.389911', '(43.642,-79.390)', '0', 'mission3', '1361932008400', '1361932008400', '5'
);

INSERT INTO `newcanuck`.`missions`
(
`approval`,`_id`,`name`,`description`,`create_date`,`ratetimes`,`latitude`,`longitude`,`address`,`state`,`img_file_name`,`add_date`,`complete_date`,`rating`)
VALUES
(
'true',4,'Coffee in Tim Hortons', 'Tim Hortons Inc. is a Canadian fast casual restaurant known for its coffee and doughnuts. It is also Canada''s largest fast food service with over 3,000 stores nationwide. It was founded in 1964 in Hamilton, Ontario, by Canadian hockey player Tim Horton and Jim Charade, after an initial venture in hamburger restaurants.', '1361932008400', '3', '43.652597', '-79.38755', '(43.653,-79.388)', '0', 'mission4', '1361932008400', '1361932008400', '3.5'
);

INSERT INTO `newcanuck`.`missions`
(
`approval`,`_id`,`name`,`description`,`create_date`,`ratetimes`,`latitude`,`longitude`,`address`,`state`,`img_file_name`,`add_date`,`complete_date`,`rating`)
VALUES
(
'true',5,'Get a Driver''s Licence', 'If you are at least 16 years old, you can apply for driver licence in Ontario.  As a new driver, you will need to practice driving and gain experience over time.  For most people, the two-step process takes about 20 months to finish.  Here how you get a driver licence. Visit http://www.ontario.ca/driving-and-roads/get-g-drivers-licence-new-drivers for more detail.', '1361932008400', '2', '43.653839', '-79.376564', '(43.654,-79.377)', '0', 'mission5', '1361932008400', '1361932008400', '3'
);

INSERT INTO `newcanuck`.`missions`
(
`approval`,`_id`,`name`,`description`,`create_date`,`ratetimes`,`latitude`,`longitude`,`address`,`state`,`img_file_name`,`add_date`,`complete_date`,`rating`)
VALUES
(
'true',6,'Visit Art Gallery of Ontario', 'The Art Gallery of Ontario (AGO)  is an art museum in Toronto''s Downtown Grange Park district, on Dundas Street West between McCaul Street and Beverley Street. \n Its collection includes more than 80,000 works spanning the 1st century to the present day. The gallery has 45,000 square metres (480,000 sq ft) of physical space, making it one of the largest galleries in North America.', '1361932008400', '4', '43.654553', '-79.3924', '(43.655,-79.392)', '0', 'mission6', '1361932008400', '1361932008400', '4'
);

INSERT INTO `newcanuck`.`comments`
(`_id`,
`mission_id`,
`user_name`,
`user_rating`,
`user_comment`,
`create_date`)
VALUES
(
1,
1,
'Marcy',
4.5,
'Maybe the best waterfall in the world!',
'1361932008400'
);

INSERT INTO `newcanuck`.`comments`
(`_id`,
`mission_id`,
`user_name`,
`user_rating`,
`user_comment`,
`create_date`)
VALUES
(
2,
1,
'Mengxi',
5,
'Remember to bring your camera..',
'1361932008400'
);

INSERT INTO `newcanuck`.`comments`
(`_id`,
`mission_id`,
`user_name`,
`user_rating`,
`user_comment`,
`create_date`)
VALUES
(
3,
1,
'Zeyu',
4.5,
'Bravo!!',
'1361932008400'
);

   
INSERT INTO `quizQuestions` VALUES (6, 'What is the meaning of Toonie?', 'Life', 'Canadian 1 dollar coin', 'Canadian 2 dollar coin', 'Canadian 50 cents coin', 'Canadian 25 cents coin', 'B', 'There are 2 colors in the coin.', 'Tonnie refers to Canadian 2 dollar coin in Canada. Loonie refers to 1 dollar coin.', 'true'); 
INSERT INTO `quizQuestions` VALUES (7, 'Which date is the national day of Canada?', 'Life', 'July 1, 1867', 'July 2, 1867', 'July 1, 1868', 'July 2, 1868', 'A', 'The first day of a month.', 'Canada Day is the national day of Canada, a federal statutory holiday celebrating the anniversary of the July 1, 1867, enactment of the British North America Act, 1867 (today called the Constitution Act, 1867), which united three colonies into a single country called Canada within the British Empire. Originally called Dominion Day, the name was changed in 1982, the year the Canada Act was passed. Canada Day observances take place throughout Canada as well as by Canadians internationally.' , 'true'); 
INSERT INTO `quizQuestions` VALUES (8, 'Who is the current Prime Minister of Canada?', 'Life', 'Stephen Harper', 'Julia Gillard', 'David Cameron', 'David Huston', 'A', 'He is a good man.', 'Stephen Joseph Harper (born April 30, 1959), PC, MP, is the twenty-second and current prime minister of Canada and leader of the Conservative Party. Harper became prime minister when his party formed a minority government after the 2006 federal election. He is the first prime minister from the newly reconstituted Conservative Party, following a merger of the Progressive Conservative and Canadian Alliance parties.' , 'true'); 
INSERT INTO `quizQuestions` VALUES (9, 'Which food is famous in Canada?', 'Life', 'Star Buck Coffee', 'Buger King', 'Red Loster', 'Poutine', 'D', 'Don not eat too much if you don not want to get fat', 'Poutine is a typical Canadian dish (originally from Quebec), made with french fries, topped with brown gravy and curd cheese. Sometimes additional ingredients are added. Poutine is a fast food dish that can now be found across Canada (and is also found in some places in the northern United States).' , 'true'); 
   
INSERT INTO `quizQuestions` VALUES (16, 'Which Province is the last Province to joined Canada?', 'History', 'Newfoundland Labrador', 'Alberta', 'Saskatchewan', 'Prince Edward Island', 'A', 'Answer is more than one word.', 'Newfoundland Labrador is the last province to join Canada. It happened in Mach 31st, 1949. Also, Nunavut, which joined Canada in April 1st, 1999, is the last territory to join Canada. ' , 'true'); 
INSERT INTO `quizQuestions` VALUES (17, 'Till now, how many Prime Ministers have served for Canada in history?', 'History', '18', '21', '22', '31', 'C', 'More than 20 less than 30.', 'Newfoundland Labrador is the last province to join Canada. It happened in Mach 31st, 1949. Also, Nunavut, which joined Canada in April 1st, 1999, is the last territory to join Canada.' , 'true'); 
INSERT INTO `quizQuestions` VALUES (18, 'Now how many provinces/territories does Canada have?', 'History', '12', '13', '17', '20', 'B', 'Not very bigger than 10', 'Canada has 10 provinces, 3 territories.  Provinces: Newfoundland and Labrador, Prince Edward, Island, Nova Scotia, New Brunswick, Quebec; Ontario; Manitoba; Saskatchewan; Alberta; British Columbia. Territories: Nunavut, North West Territories, Yukon.', 'true' ); 
INSERT INTO `quizQuestions` VALUES (19, 'Which province in the below is not a province of Canada in 1868?', 'History', 'Ontario', 'Qu¨¦bec', 'Nova Scotia', 'British Columbia', 'D', 'It is not in the west of Canada.', 'In 1867, Canada had only four provinces, and they are Ontario, Qu¨¦bec, Nova Scotia and New Brunswick. They are all located in the east of Canada.' , 'true'); 
 
Status API Training Shop Blog About © 2013 GitHub, Inc. Terms Privacy Security Contact 