package newcanuck.entity;

import java.io.Serializable;
import java.util.Date;

public class Feeling implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;

	private Date createDate;

	private Long missionId;
	private String missionName;
	private String missionDescription;
	private String missionAddress;
	private String missionLatitude;
	private String missionLongitude;
	
	private String myRating;
	private String myFeeling;
	private String myImgFileName;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getMissionId() {
		return missionId;
	}
	
	public void setMissionId(Long missionId) {
		this.missionId = missionId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getMissionName() {
		return missionName;
	}

	public void setMissionName(String missionName) {
		this.missionName = missionName;
	}

	public String getMyRating() {
		return myRating;
	}

	public void setMyRating(String myRating) {
		this.myRating = myRating;
	}

	public String getMyFeeling() {
		return myFeeling;
	}

	public void setMyFeeling(String myFeeling) {
		this.myFeeling = myFeeling;
	}

	public String getMyImgFileName() {
		return myImgFileName;
	}

	public void setMyImgFileName(String myImgFileName) {
		this.myImgFileName = myImgFileName;
	}

	public String getMissionDescription() {
		return missionDescription;
	}

	public void setMissionDescription(String missionDescription) {
		this.missionDescription = missionDescription;
	}

	public String getMissionAddress() {
		return missionAddress;
	}

	public void setMissionAddress(String missionAddress) {
		this.missionAddress = missionAddress;
	}

	public String getMissionLatitude() {
		return missionLatitude;
	}

	public void setMissionLatitude(String missionLatitude) {
		this.missionLatitude = missionLatitude;
	}

	public String getMissionLongitude() {
		return missionLongitude;
	}

	public void setMissionLongitude(String missionLongitude) {
		this.missionLongitude = missionLongitude;
	}
}
