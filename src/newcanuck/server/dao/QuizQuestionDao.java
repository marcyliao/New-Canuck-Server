package newcanuck.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import newcanuck.entity.Comment;
import newcanuck.entity.QuizQuestion;

public class QuizQuestionDao {
	private SQLHelper helper = new SQLHelper();

	Connection connection = null;
	PreparedStatement ptmt = null;
	ResultSet rs = null;
	
	public void insertQuizQuestion(QuizQuestion question) {
		try {
			String queryString = "INSERT INTO quizQuestions" +
					"(description, type, answerA, answerB, answerC, answerD, correctAnswer, hint, suggestion)" +
					"VALUES(?,?,?,?,?,?,?,?,?)";
			
			Connection connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setString(1, question.getDescription());
			ptmt.setString(2, question.getType());
			ptmt.setString(3, question.getAnswerA());
			ptmt.setString(4, question.getAnswerB());
			ptmt.setString(5, question.getAnswerC());
			ptmt.setString(6, question.getAnswerD());
			ptmt.setString(7, question.getCorrectAnswer());
			ptmt.setString(8, question.getHint());
			ptmt.setString(9, question.getSuggestion());
			ptmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			closeAll();
		}
	}
	
	public void updateQuizQuestion(QuizQuestion question) {
		try {
 			String queryString = "UPDATE quizQuestions" +
					" set description=?, type=?, answerA=?, answerB=?, answerC=?, answerD=?, correctAnswer=?, hint=?, suggestion=?, approval=? " +
					"where id=?";
			
			Connection connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setString(1, question.getDescription());
			ptmt.setString(2, question.getType());
			ptmt.setString(3, question.getAnswerA());
			ptmt.setString(4, question.getAnswerB());
			ptmt.setString(5, question.getAnswerC());
			ptmt.setString(6, question.getAnswerD());
			ptmt.setString(7, question.getCorrectAnswer());
			ptmt.setString(8, question.getHint());
			ptmt.setString(9, question.getSuggestion());
			ptmt.setString(10, question.getApproval());
			ptmt.setLong(11, question.getId());
			ptmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			closeAll();
		}
	}
	
	public QuizQuestion getQuizQuestion(Long id){
		try {
			String queryString = "SELECT * FROM quizQuestions where id=?";
			connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setLong(1, id);
			rs = ptmt.executeQuery();

			QuizQuestion question = new QuizQuestion();
			if (rs.next()) {
				question.setId(rs.getLong(1));
				question.setDescription(rs.getString(2));
				question.setType(rs.getString(3));
				question.setAnswerA(rs.getString(4));
				question.setAnswerC(rs.getString(6));
				question.setAnswerB(rs.getString(5));
				question.setAnswerD(rs.getString(7));
				question.setCorrectAnswer(rs.getString(8));
				question.setHint(rs.getString(9));
				question.setSuggestion(rs.getString(10));
				question.setApproval(rs.getString(11));
				
			}

			return question;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			closeAll();
		}
		return null;
	}
	
	public List<QuizQuestion> getAllQuestionsByApproval(String approval){
		try {
			String queryString = "SELECT * FROM quizQuestions where approval=?";
			connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setString(1, approval);
			rs = ptmt.executeQuery();

			List<QuizQuestion> qustions = new ArrayList<QuizQuestion>();
			while (rs.next()) {
				QuizQuestion question = new QuizQuestion();
				question.setId(rs.getLong(1));
				question.setDescription(rs.getString(2));
				question.setType(rs.getString(3));
				question.setAnswerA(rs.getString(4));
				question.setAnswerC(rs.getString(6));
				question.setAnswerB(rs.getString(5));
				question.setAnswerD(rs.getString(7));
				question.setCorrectAnswer(rs.getString(8));
				question.setHint(rs.getString(9));
				question.setSuggestion(rs.getString(10));
				question.setApproval(rs.getString(11));
				
				qustions.add(question);
			}

			return qustions;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			closeAll();
		}
		return null;
	}
	public List<QuizQuestion> getAllQuestions(){
		try {
			String queryString = "SELECT * FROM quizQuestions";
			connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			rs = ptmt.executeQuery();

			List<QuizQuestion> qustions = new ArrayList<QuizQuestion>();
			while (rs.next()) {
				QuizQuestion question = new QuizQuestion();
				question.setId(rs.getLong(1));
				question.setDescription(rs.getString(2));
				question.setType(rs.getString(3));
				question.setAnswerA(rs.getString(4));
				question.setAnswerC(rs.getString(6));
				question.setAnswerB(rs.getString(5));
				question.setAnswerD(rs.getString(7));
				question.setCorrectAnswer(rs.getString(8));
				question.setHint(rs.getString(9));
				question.setSuggestion(rs.getString(10));
				question.setApproval(rs.getString(11));
				
				qustions.add(question);
			}

			return qustions;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			closeAll();
		}
		return null;
	}
	private void closeAll(){
		try {
            if (ptmt != null)
           	 ptmt.close();
            if (connection != null)
                connection.close();
		} catch (SQLException e) {
            e.printStackTrace();
		} catch (Exception e) {
            e.printStackTrace();
		}
	}
}
