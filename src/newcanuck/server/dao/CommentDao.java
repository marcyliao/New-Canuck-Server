package newcanuck.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import newcanuck.entity.Comment;

public class CommentDao {
	private SQLHelper helper = new SQLHelper();

	Connection connection = null;
	PreparedStatement ptmt = null;
	ResultSet rs = null;
	
	public void insertComment(Comment comment) {
		try {
			String queryString = "INSERT INTO comments" +
					"(mission_id,user_name,user_rating,user_comment,create_date)" +
					"VALUES(?,?,?,?,?)";
			Connection connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setLong(1, comment.getMissionId());
			ptmt.setString(2, comment.getUserName());
			ptmt.setDouble(3, comment.getUserRating());
			ptmt.setString(4, comment.getUserComment());
			ptmt.setLong(5, comment.getCreateDate());
			ptmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			closeAll();
		}
	}
	
	public List<Comment> getComments(Long missionId){
		try {
			String queryString = "SELECT * FROM comments where mission_id = ?";
			connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setLong(1, missionId);
			rs = ptmt.executeQuery();

			List<Comment> comments = new ArrayList<Comment>();
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getLong(1));
				comment.setMissionId(rs.getLong(2));
				comment.setUserName(rs.getString(3));
				comment.setUserRating(rs.getDouble(4));
				comment.setUserComment(rs.getString(5));
				comment.setCreateDate(rs.getLong(6));
				comments.add(comment);
			}

			return comments;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			closeAll();
		}
		return null;
	}
	
	private void closeAll(){
		try {
            if (ptmt != null)
           	 ptmt.close();
            if (connection != null)
                connection.close();
		} catch (SQLException e) {
            e.printStackTrace();
		} catch (Exception e) {
            e.printStackTrace();
		}
	}
}
