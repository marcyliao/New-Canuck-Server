package newcanuck.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import newcanuck.entity.Mission;

public class MissionDao {
	private SQLHelper helper = new SQLHelper();

	Connection connection = null;
	PreparedStatement ptmt = null;
	ResultSet rs = null;

	public void updateMission(Mission mission) {
		try {
			String queryString = "UPDATE missions" +
					" set name=?,description=?,create_date=?,ratetimes=?,latitude=?,longitude=?,address=?,state=?,img_file_name=?,add_date=?,complete_date=?,rating=?,approval=?" +
					" where _id=?";
			Connection connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setString(1, mission.getName());
			ptmt.setString(2, mission.getDescription());
			ptmt.setLong(3, mission.getCreateDate());
			ptmt.setLong(4, mission.getRatetimes());
			ptmt.setDouble(5, mission.getLatitude());
			ptmt.setDouble(6, mission.getLongitude());
			ptmt.setString(7, mission.getAddress());
			ptmt.setLong(8, mission.getState());
			ptmt.setString(9, mission.getImgFileName());
			ptmt.setLong(10, mission.getAddDate());
			ptmt.setLong(11, mission.getCompleteDate());
			ptmt.setDouble(12, mission.getRating());
			ptmt.setString(13, mission.getApproval());
			ptmt.setLong(14, mission.getId());
			ptmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			closeAll();
		}
	}
	
	public void insertMission(Mission mission) {
		try {
			String queryString = "INSERT INTO missions" +
					"(name,description,create_date,ratetimes,latitude,longitude,address,state,img_file_name,add_date,complete_date,rating,approval)" +
					"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Connection connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setString(1, mission.getName());
			ptmt.setString(2, mission.getDescription());
			ptmt.setLong(3, mission.getCreateDate());
			ptmt.setLong(4, mission.getRatetimes());
			ptmt.setDouble(5, mission.getLatitude());
			ptmt.setDouble(6, mission.getLongitude());
			ptmt.setString(7, mission.getAddress());
			ptmt.setLong(8, mission.getState());
			ptmt.setString(9, mission.getImgFileName());
			ptmt.setLong(10, mission.getAddDate());
			ptmt.setLong(11, mission.getCompleteDate());
			ptmt.setDouble(12, mission.getRating());
			ptmt.setString(13, mission.getApproval());
			ptmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			closeAll();
		}
	}

	public List<Mission> getAllMissionsByApproval(String approval) {
		try {
			String queryString = "SELECT * FROM missions where approval=?";
			connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setString(1, approval);
			rs = ptmt.executeQuery();

			List<Mission> missions = new ArrayList<Mission>();
			while (rs.next()) {
				Mission mission = new Mission();
				mission.setId(rs.getLong(1));
				mission.setName(rs.getString(2));
				mission.setDescription(rs.getString(3));
				mission.setCreateDate(rs.getLong(4));
				mission.setRatetimes(rs.getLong(5));
				mission.setLatitude(rs.getDouble(6));
				mission.setLongitude(rs.getDouble(7));
				mission.setAddress(rs.getString(8));
				mission.setState(rs.getLong(9));
				mission.setImgFileName(rs.getString(10));
				mission.setAddDate(rs.getLong(11));
				mission.setCompleteDate(rs.getLong(12));
				mission.setRating(rs.getDouble(13));
				mission.setApproval(rs.getString(14));
				missions.add(mission);
			}

			return missions;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			closeAll();
		}
		return null;
	}
	
	public Mission getMission(Long id) {
		try {
			String queryString = "SELECT * FROM missions where _id=?";
			connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			ptmt.setLong(1, id);
			rs = ptmt.executeQuery();

			Mission mission = null;
			if (rs.next()) {
				mission = new Mission();
				mission.setId(rs.getLong(1));
				mission.setName(rs.getString(2));
				mission.setDescription(rs.getString(3));
				mission.setCreateDate(rs.getLong(4));
				mission.setRatetimes(rs.getLong(5));
				mission.setLatitude(rs.getDouble(6));
				mission.setLongitude(rs.getDouble(7));
				mission.setAddress(rs.getString(8));
				mission.setState(rs.getLong(9));
				mission.setImgFileName(rs.getString(10));
				mission.setAddDate(rs.getLong(11));
				mission.setCompleteDate(rs.getLong(12));
				mission.setRating(rs.getDouble(13));
				mission.setApproval(rs.getString(14));
			}

			return mission;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			closeAll();
		}
		return null;
	}
	
	public List<Mission> getAllMissions() {
		try {
			String queryString = "SELECT * FROM missions";
			connection = helper.getConn();
			ptmt = connection.prepareStatement(queryString);
			rs = ptmt.executeQuery();

			List<Mission> missions = new ArrayList<Mission>();
			while (rs.next()) {
				Mission mission = new Mission();
				mission.setId(rs.getLong(1));
				mission.setName(rs.getString(2));
				mission.setDescription(rs.getString(3));
				mission.setCreateDate(rs.getLong(4));
				mission.setRatetimes(rs.getLong(5));
				mission.setLatitude(rs.getDouble(6));
				mission.setLongitude(rs.getDouble(7));
				mission.setAddress(rs.getString(8));
				mission.setState(rs.getLong(9));
				mission.setImgFileName(rs.getString(10));
				mission.setAddDate(rs.getLong(11));
				mission.setCompleteDate(rs.getLong(12));
				mission.setRating(rs.getDouble(13));
				mission.setApproval(rs.getString(14));
				missions.add(mission);
			}

			return missions;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			closeAll();
		}
		return null;
	}
	
	private void closeAll(){
		try {
            if (ptmt != null)
           	 ptmt.close();
            if (connection != null)
                connection.close();
		} catch (SQLException e) {
            e.printStackTrace();
		} catch (Exception e) {
            e.printStackTrace();
		}
	}
}
