package newcanuck.server.actions;

import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import newcanuck.entity.*;
import newcanuck.server.dao.MissionDao;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class MissionAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {
	
	MissionDao dao = new MissionDao();
    
	private static final long serialVersionUID = 1L;
	private HttpServletRequest request;
    private HttpServletResponse response;
    
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	public void getAllMissions(){
		try {
            this.response.setContentType("text/html;charset=utf-8");
            this.response.setCharacterEncoding("UTF-8");
            
            List<Mission> missions = dao.getAllMissionsByApproval("true");
             
            JSONArray json = JSONArray.fromObject(missions);   
            this.response.getWriter().write(json.toString());
            
       } catch (Exception e) { 
           e.printStackTrace();
       }
	}
	
	private List<Mission> missions;
	public List<Mission> getMissions(){
		return missions;
	}
	
	public void setMissions(List<Mission> missions){
		this.missions = missions;
	}
	
	private String username;
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getMissionManagementUI(){
		
		Map<String,Object> session = ServletActionContext.getContext().getSession();
		username = (String) session.get("user");
		
		if(username != null){
			missions = dao.getAllMissions();
			return SUCCESS;
		}
		
		addActionError("please login first");
		return ERROR;
	}
	
	public String approveMission() {
		this.response.setContentType("text/html;charset=utf-8");
		this.response.setCharacterEncoding("UTF-8");
		
		Long id = Long.parseLong(request.getParameter("id"));
		Mission mission = dao.getMission(id);
		mission.setApproval("true");
		dao.updateMission(mission);
		
		return SUCCESS;
	}
	
	public String rejectMission() {
		this.response.setContentType("text/html;charset=utf-8");
		this.response.setCharacterEncoding("UTF-8");
		
		Long id = Long.parseLong(request.getParameter("id"));
		Mission mission = dao.getMission(id);
		mission.setApproval("false");
		dao.updateMission(mission);
		
		return SUCCESS;
	}
	
	public void postMission(){
		 this.response.setContentType("text/html;charset=utf-8");
         this.response.setCharacterEncoding("UTF-8");
         
         Mission mission = new Mission();
         mission.setName(request.getParameter("name"));
         mission.setDescription(request.getParameter("description"));
         mission.setCreateDate(Long.parseLong(request.getParameter("create_date")));
         mission.setLatitude(Double.parseDouble(request.getParameter("latitude")));
         mission.setLongitude(Double.parseDouble(request.getParameter("longitude")));
         mission.setAddress(request.getParameter("address"));
         mission.setRatetimes(0L);
         mission.setRating(3D);
         mission.setState(0L);
         mission.setAddDate(new Date().getTime());
         mission.setCompleteDate(new Date().getTime());
         mission.setApproval("false");
         
         String fileName = UUID.randomUUID().toString();
         mission.setImgFileName(fileName);
         
         String image = this.request.getParameter("image");
         byte [] bytes = Base64.decode(image);
         try {
        	 String largeImgPath = ServletActionContext.getServletContext().getRealPath("/user_images/l/"+fileName+".jpg");
        	 String smallImgPath = ServletActionContext.getServletContext().getRealPath("/user_images/s/"+fileName+"_s.jpg");
        	 
        	 File file = new File(largeImgPath);
        	 file.createNewFile();
             FileOutputStream fs = new FileOutputStream(file);
             BufferedOutputStream bs = new BufferedOutputStream(fs);
             bs.write(bytes);
             bs.close();
             
             File sImg = new File(smallImgPath);
             BufferedImage Bi = ImageIO.read(file);
             Image Itemp = Bi.getScaledInstance(200, 200, BufferedImage.SCALE_DEFAULT);  
             double Ratio = 0.0;  
             if ((Bi.getHeight() > 130) || (Bi.getWidth() > 80)) {  
                 if (Bi.getHeight() > Bi.getWidth())  
                     Ratio = 80.0 / Bi.getHeight();  
                 else  
                     Ratio = 130.0 / Bi.getWidth();  
                 AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(Ratio, Ratio), null);  
                 Itemp = op.filter(Bi, null);  
             }  
   
             ImageIO.write((BufferedImage) Itemp, "jpg", sImg);
             dao.insertMission(mission);
             
             this.response.getWriter().write("Create new mission successfully!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
