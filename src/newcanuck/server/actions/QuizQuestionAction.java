package newcanuck.server.actions;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import newcanuck.entity.QuizQuestion;
import newcanuck.server.dao.QuizQuestionDao;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;

public class QuizQuestionAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {
	QuizQuestionDao dao = new QuizQuestionDao();

	private static final long serialVersionUID = 1L;
	private HttpServletRequest request;
	private HttpServletResponse response;

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	private List<QuizQuestion> questions;
	public void setQuestions(List<QuizQuestion> questions){
		this.questions = questions;
	}
	
	public List<QuizQuestion> getQuestions(){
		return questions;
	}
	
	public String getQuizQuestionsInWeb(){
		try {
            this.response.setContentType("text/html;charset=utf-8");
            this.response.setCharacterEncoding("UTF-8");
            
            String approval = request.getParameter("approval");
     
            questions = dao.getAllQuestionsByApproval(approval);
             
       } catch (Exception e) { 
           e.printStackTrace();
       }
       return SUCCESS;
	}
	
	public void getAllQuizQuestions(){
		try {
            this.response.setContentType("text/html;charset=utf-8");
            this.response.setCharacterEncoding("UTF-8");
            
            String approval = "true";
            
            List<QuizQuestion> questions = dao.getAllQuestionsByApproval(approval);
             
            JSONArray json = JSONArray.fromObject(questions);   
            this.response.getWriter().write(json.toString());
            
       } catch (Exception e) { 
           e.printStackTrace();
       }
	}
	
	public String approveQuizQuestion() {
		this.response.setContentType("text/html;charset=utf-8");
		this.response.setCharacterEncoding("UTF-8");
		
		Long id = Long.parseLong(request.getParameter("id"));
		QuizQuestion question = dao.getQuizQuestion(id);
		question.setApproval("true");
		dao.updateQuizQuestion(question);
		
		return SUCCESS;
	}
	
	public String rejectQuizQuestion() {
		this.response.setContentType("text/html;charset=utf-8");
		this.response.setCharacterEncoding("UTF-8");
		
		Long id = Long.parseLong(request.getParameter("id"));
		QuizQuestion question = dao.getQuizQuestion(id);
		question.setApproval("false");
		dao.updateQuizQuestion(question);
		
		return SUCCESS;
	}
	
	
	private String username;
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getUsername(){
		return username;
	}
	public String getQuizManagementUI() {
		
		Map<String,Object> session = ServletActionContext.getContext().getSession();
		username = (String) session.get("user");
		
		if(username != null){
			questions = dao.getAllQuestions();
			return SUCCESS;
		}
		
		addActionError("please login first");
		return ERROR;
	}
	
	public void postQuizQuestion(){
		this.response.setContentType("text/html;charset=utf-8");
		this.response.setCharacterEncoding("UTF-8");

		QuizQuestion question = new QuizQuestion();
		question.setDescription(request.getParameter("description"));
		question.setType(request.getParameter("type"));
		question.setAnswerA(request.getParameter("answerA"));
		question.setAnswerB(request.getParameter("answerB"));
		question.setAnswerC(request.getParameter("answerC"));
		question.setAnswerD(request.getParameter("answerD"));
		question.setCorrectAnswer(request.getParameter("correctAnswer"));
		question.setHint(request.getParameter("hint"));
		question.setSuggestion(request.getParameter("suggestion"));
		question.setApproval("false");
		
		try {
			dao.insertQuizQuestion(question);
			this.response.getWriter().write("Upload question successfully! Thank you for your contribution!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}









