package newcanuck.server.actions;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import newcanuck.entity.Comment;
import newcanuck.server.dao.CommentDao;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;

public class CommentAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {
	CommentDao dao = new CommentDao();

	private static final long serialVersionUID = 1L;
	private HttpServletRequest request;
	private HttpServletResponse response;

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	public void getComments(){
		try {
            this.response.setContentType("text/html;charset=utf-8");
            this.response.setCharacterEncoding("UTF-8");
            
            Long missionId = Long.parseLong(request.getParameter("missionId"));
            
            List<Comment> comments = dao.getComments(missionId);
             
            JSONArray json = JSONArray.fromObject(comments);   
            this.response.getWriter().write(json.toString());
            
       } catch (Exception e) { 
           e.printStackTrace();
       }
	}
	
	public void postComment() {
		this.response.setContentType("text/html;charset=utf-8");
		this.response.setCharacterEncoding("UTF-8");

		Comment comment = new Comment();
		comment.setMissionId(Long.parseLong(request.getParameter("missionId")));
		comment.setUserName(request.getParameter("userName"));
		comment.setUserComment(request.getParameter("comment"));
		comment.setUserRating(Double.parseDouble(request.getParameter("rating")));
		comment.setCreateDate(Long.parseLong(request.getParameter("createDate")));
		
		try {
			dao.insertComment(comment);
			this.response.getWriter().write("Share feeling successfully!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}









