package newcanuck.server.actions;

import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class HomeAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String login(){
		if(username!= null && password!=null && username.equals("Bill") && password.equals("12345")) {
	    	Map<String,Object> session = ServletActionContext.getContext().getSession();
			session.put("user", username);
			return SUCCESS;
		}

		addActionError("username and password not match");
		return ERROR;
	}
}
