<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
body {
	background-image: url("https://www.coursepeer.com/images/bg.png");
	background-color: #9fd5f7;
	font-family: 'Helvetica Nueve', Helvetica, sans-serif;
}

.user {
	margin: 10px;
	font-size: 20px;
}

h1 {
	color: #3366FF;
	font-size: 48px;
	margin-bottom: 5px;
	margin-top: 32px;
}

.navi {
	margin: 10px;
	font-size: 24px;
}

table {
	margin: 10px;
	padding: 5px;
	border: 1px solid black;
	border-collapse: collapse;
	background-color: white;
}

table th,td {
	border: 1px solid black;
	padding: 5px;
}

table .short {
	width: 100px;
}

.icon {
	width: 50px;
	height: 50px;
}

table .middle {
	width: 200px;
}

#form1__login {
	width: 100%;
}
</style>
</head>

<body>
	<h1>NewCanuck Management Platform</h1>
	<s:actionerror />
	<s:form action="login.action" method="post" id="form1">
		<s:textfield name="username" label="Username" />
		<s:password name="password" label="Password" />
		<s:submit method="login" value="Login" align="center" />
	</s:form>
</body>
</html>
