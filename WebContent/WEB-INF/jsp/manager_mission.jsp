<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=ISO-8859-1">
		<title>Manager UI</title>
		<style type="text/css">
		body{
			background-image: url("https://www.coursepeer.com/images/bg.png");
			background-color: #9fd5f7;
            font-family: 'Helvetica Nueve', Helvetica, sans-serif;
		}
				
		.user{
			float:right;
			font-size:20px;
			color:white;
		}
		
		h1 {
			color:#3366FF;
			font-size:48px;
			margin-bottom:5px;
		}
		
		table {
			margin:10px;
			padding:5px;
			border: 1px solid black;
			border-collapse:collapse;
			background-color:white;
		}
		
		table th, td {
			border: 1px solid black;
			padding:5px;
		}
		
		table .short{
			width:100px;
		}
		
		table .middle{
			width:200px;
		}
		
		.icon {
			width:50px;
			height:50px;
		}
		
		.logo {
			width:100px;
		}
		
		.navi {
			background-image: url("/NewCanuck/image/navi.png");
			margin-left:10px;
			margin-right:10px;
			margin-top:10px;
			margin-bottom:0px;
			padding:10px 15px;
			border-radius: 10px 10px 0px 0px;
			color:#999;
		}

		.navi a{
			color: white;
			font-size:18px;
			font-weight:300;
			text-decoration: none;
			margin-left:10px;
			margin-right:10px;
		}
		
		.navi .select{
			color: yellow;
			font-size:18px;
			font-weight:300;
			text-decoration: none;
		}
		
		.list {
			margin-top:0px;
		}
		</style>
	</head>
	<body>
		<h1>NewCanuck Management Platform</h1>
		<div class="navi">
			<a class="select" href="/NewCanuck/getMissionManagementUI.action"> Missions </a>
			|<a href="/NewCanuck/getQuizManagementUI.action"> Quiz Questions </a>
			|<a href="/NewCanuck/upload_mission.jsp">Upload Missions</a>
			|<a href="/NewCanuck/upload_question.jsp">Upload Questions</a>		
			|<a href="/NewCanuck/"> Logout </a>
		<span class="user">Hello, <s:property value="username" />!</span>
		</div >
		<table class="list">
			<thead>
				<tr>
					<th>
						Logo
					</th>
					<th class="short">
						Mission Name
					</th>
					<th>
						Mission Description
					</th>
					<th class="short">
						Create Date
					</th>
					<th class="short">
						Rate Times
					</th>
					<th class="short">
						Rating
					</th>
					<th class="short">
						Latitude
					</th>
					<th class="short">
						Longitude
					</th>
					<th class="short">
						Operations
					</th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="missions">
					<tr>
						<td>
							<img  class="logo" src="/NewCanuck/user_images/s/<s:property value="imgFileName" />_s.jpg">
						</td>
						<td>
							<s:property value="name" />
						</td>
						<td>
							<s:property value="description" />
						</td>
						<td>
							<s:property value="createDate" />
						</td>
						<td>
							<s:property value="ratetimes" />
						</td>
						<td>
							<s:property value="rating" />
						</td>
						<td>
							<s:property value="latitude" />
						</td>
						<td>
							<s:property value="longitude" />
						</td>
						<td>
							<s:if test="%{approval=='true'}">
								<img class="icon" src="/NewCanuck/image/true.png"/><br>
								<a href="/NewCanuck/rejectMission?id=<s:property value="id" />">
									Reject
								</a>
							</s:if>
							<s:elseif test="%{approval=='false'}">
								<img class="icon" src="/NewCanuck/image/false.png"/><br>
								<a href="/NewCanuck/approveMission?id=<s:property value="id" />">
									Approve
								</a>
							</s:elseif>
						</td>
					</tr>
				</s:iterator>
			</tbody>
		</table>

	</body>
</html>