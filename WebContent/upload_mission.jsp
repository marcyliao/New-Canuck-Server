<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Upload Question</title>
<style type="text/css">
body {
	background-image: url("https://www.coursepeer.com/images/bg.png");
	background-color: #9fd5f7;
	font-family: 'Helvetica Nueve', Helvetica, sans-serif;
}

h1 {
	color: #3366FF;
	font-size: 48px;
	margin-bottom: 5px;
	margin-top: 32px;
}

table {
	margin-left: 10px;
	padding: 5px;
	border: 1px solid black;
	border-collapse: collapse;
	border-spacing: 0;
	background-color: white;
}

table th,td {
	border: 1px solid black;
	padding: 5px;
}

.navi {
	background-image: url("/NewCanuck/image/navi.png");
	margin-left: 10px;
	margin-right: 10px;
	margin-top: 10px;
	margin-bottom: 0px;
	padding: 10px 15px;
	border-radius: 10px 10px 0px 0px;
	color: #999;
}

.navi a {
	color: white;
	font-size: 18px;
	font-weight: 300;
	text-decoration: none;
	margin-left: 10px;
	margin-right: 10px;
}

.navi .select {
	color: yellow;
	font-size: 18px;
	font-weight: 300;
	text-decoration: none;
}

.list {
	margin-top: 0px;
}

.submit {
	border-top: none;
	text-align: center;
	margin-top: 0px;

}

</style>
</head>

<body>
	<h1>NewCanuck Management Platform</h1>
	<div class="navi">
		<a href="/NewCanuck/getMissionManagementUI.action"> Missions </a> |<a
			href="/NewCanuck/getQuizManagementUI.action"> Quiz Questions </a> |<a
			class="select" href="/NewCanuck/upload_mission.jsp">Upload
			Missions</a> |<a href="/NewCanuck/upload_question.jsp">Upload
			Questions</a> |<a href="/NewCanuck/"> Logout </a>
	</div>
	<form action="/NewCanuck/upload_mission.jsp" method="post">
		<table class="list" width="98.4%">
			<tr>
				<td style="width:25%">Mission Name</td>
				<td style="width:75%"><input style="width:30%" type="text"
					name="Mission Name"></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><textarea name="Description" rows="5" cols="80"></textarea>
				</td>
			</tr>
			<tr>
				<td>Latitude</td>
				<td><input style="width:30%" type="text" name="Latitude">
				</td>
			</tr>
			<tr>
				<td>Longitude</td>
				<td><input style="width:30%" type="text" name="Longitude">
				</td>
			</tr>
		</table>
		<table class="submit" width="98.4%">
			<tr>
				<td><input type="file" name="file1" id="file1" />
				</td>
			</tr>
			<tr>
				<td style="width:100%"><input type="submit" value="Submit">
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
